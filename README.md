[Running every friday at 4:00 AM UTC-5](https://gitlab.com/dune-archiso/testing/dune-pypi/-/pipeline_schedules).

<!-- ```
$ libsuperlu-dev libsuitesparse-dev petsc-dev paraview python-paraview gmsh libboost-all-dev
$ python -m venv dune-env
$ source dune-env/bin/activate
$ pip install --upgrade pip
$ pip install matplotlib scipy gmsh nbconvert
$ source dune-env/bin/activate
$ pip install dune-mmesh
``` -->